<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/common/include.php');
?>

<div id='page'>
  <div id='radial-menu'>
    <span class='fa-stack fa-2x' id='filter-icon' onClick='toggle_radial()'>
      <i class='far fa-stack-2x fa-circle'></i>
      <i class='fas fa-stack-1x fa-filter'></i>
    </span>
    <span class='fa-stack fa-2x sub-menu' id='community-icon' onClick='filter(this)' style='display: none'>
      <i class='far fa-stack-2x fa-circle'></i>
      <i class='fas fa-stack-1x fa-users'></i>
    </span>
    <span class='fa-stack fa-2x sub-menu' id='food-icon' onClick='filter(this)' style='display: none'>
      <i class='far fa-stack-2x fa-circle'></i>
      <i class='fas fa-stack-1x fa-utensils'></i>
    </span>
    <span class='fa-stack fa-2x sub-menu' id='shop-icon' onClick='filter(this)' style='display: none'>
      <i class='far fa-stack-2x fa-circle'></i>
      <i class='fas fa-stack-1x fa-store-alt'></i>
    </span>
  </div>
</div>



<style>
  #page {
    border: 2px solid black;
    height: 60%;
    position: relative;
    width: 70%;
  }

  #filter-icon {
    bottom: 0;
    position: absolute;
    right: 0;
  }

  #community-icon {
    bottom: 0;
    right: 80px;
  }

  #food-icon {
    bottom: 55.5px;
    right: 55.5px;
  }

  #shop-icon {
    bottom: 80px;
    right: 0;
  }

  .sub-menu {
    position: absolute;
  }
</style>



<script>
  function toggle_radial() {
    if ($('.sub-menu')[0].style.display == 'none') {
      $('.sub-menu').each(function () {
        this.style.display = '';
      });
    } else {
      $('.sub-menu').each(function () {
        this.style.display = 'none';
      });
    }
  }

  function filter(icon) {
    console.log(icon.id);
  }
</script>
<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/common/include.php');
?>

<html>
<div id='page'>
  <div id='view-menu'>
    <span>
      <span class='fa-stack fa-2x view-change' id='map-view' onClick='toggle_view(this)'>
        <i class='fas fa-stack-2x fa-square'></i>
        <i class='fas fa-stack-1x fa-map-marked-alt fa-inverse'></i>
      </span>
      <span class='fa-stack fa-2x view-change' id='list-view' onClick='toggle_view(this)'>
        <i class='far fa-stack-2x fa-square'></i>
        <i class='fas fa-stack-1x fa-bars'></i>
      </span>
    </span>
  </div>
</div>

</html>



<style>
  #page {
    border: 2px solid black;
    height: 60%;
    position: relative;
    width: 70%;
  }

  #view-menu {
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%);
  }
</style>



<script>
  function toggle_view(view_icon) {
    if ($('#map-view i.fa-inverse').length && view_icon.id == 'list-view') {
      $('#map-view i.fa-inverse').removeClass('fa-inverse');
      $('#list-view i.fa-bars').addClass('fa-inverse');
      $('#map-view i.fa-square').removeClass('fas');
      $('#map-view i.fa-square').addClass('far');
      $('#list-view i.fa-square').removeClass('far');
      $('#list-view i.fa-square').addClass('fas');
    } else if (view_icon.id == 'map-view') {
      $('#list-view i.fa-inverse').removeClass('fa-inverse');
      $('#map-view i.fa-map-marked-alt').addClass('fa-inverse');
      $('#list-view i.fa-square').removeClass('fas');
      $('#list-view i.fa-square').addClass('far');
      $('#map-view i.fa-square').removeClass('far');
      $('#map-view i.fa-square').addClass('fas');
    }
  }
</script>
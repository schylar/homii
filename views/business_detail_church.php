<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/common/include.php');
?>

<div class='col-sm-12'>
  <div class='col-sm-3'>
    <img alt='Business Photo' height='250' width='100%' src='../img/church_icon.jpg'>
  </div>
  <div class='col-sm-6' id='brief-description'>
    <h3>Daar Ul-Islam Mosque</h3>
    <p>We serve the local community with a variety of services including regular congregational prayers, educational programs, workshops, burial preparations, as well as social events in accordance to the teachings of Islam.</p>
  </div>
  <div class='col-sm-3'>
    <img alt='Map' height='250' width='100%'>
  </div>
  <div class='col-sm-12' id='people-list'>
    <img class='profile-pic' src='../img/church_user1.png' alt='profile_pic' />
    <img class='profile-pic' src='../img/church_user2.png' alt='profile_pic' />
    <img class='profile-pic' src='../img/church_user3.jpeg' alt='profile_pic' />
    <img class='profile-pic' src='../img/church_user4.png' alt='profile_pic' />
  </div>
  <div class='col-sm-8 col-sm-offset-2' id='comments'>
    <div class='comment'>
    <img class='profile-pic' src='../img/church_comment1.jpeg' alt='profile_pic' />
      Best best best clean n beautiful luv the place ❤️
    </div>
    <div class='comment'>
      <img class='profile-pic' src='../img/church_comment2.jpeg' alt='profile_pic' />
      I love it i always feel peace very clean good people may Allah bless all of us
    </div>
  </div>
</div>



<style>
  #brief-description {
    text-align: center;
  }

  #people-list {
    border-top: 1px solid black;
    border-bottom: 1px solid black;
    text-align: center;
    padding: 5px;
  }

  .comment {
    border: 1px solid black;
    padding: 5px;
    margin: 10px;
  }

  .profile-pic {
    height: 50px;
    width: 50px;
    border-radius: 50%;
  }
</style>
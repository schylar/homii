<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/common/include.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/common/header.php');
?>
 
<script src='../js/niceCountryInput.js'></script>
<link rel='stylesheet' type='text/css' href='../css/niceCountryInput.css'>

<div class='col-sm-12' style='text-align: center;'>
  <div class='col-sm-12'>
    <img src='../img/homebits.png' alt='LOGO'>
  </div>

  <div class='col-sm-4 col-sm-offset-4'>
    <div class='niceCountryInputSelector' data-selectedcountry='US'  data-onchangecallback='onChangeCallback'></div>
  </div>

  <div class='col-sm-12'>
    <a href='./map_view.php?code=BE'>
      <i class='fas fa-2x fa-arrow-circle-right'></i>
    </a>
  </div>

  <div class='col-sm-12'>
    <a class='btn btn-primary' href='./map_view.php?code=BE'>
      <i class='far fa-compass'></i> Use My Location
    </a>
  </div>
</div>


<style>
  .logo {
    width: 200px;
    height: 200px;
  }

  .col-sm-12 {
    padding: 10px;
  }
</style>



<script>
  function onChangeCallback(ctr){
    console.log('The country was changed: ' + ctr);
  }
  $(document).ready(function () {
    $('.niceCountryInputSelector').each(function(i,e){
      new NiceCountryInput(e).init();
    });
  });
</script>
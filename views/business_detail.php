<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/common/include.php');
?>

<div class='col-sm-12'>
  <div class='col-sm-3'>
    <img alt='Business Photo' src='../img/food_icon.png' height='250' width='100%'>
  </div>
  <div class='col-sm-6' id='brief-description'>
    <h3>King & I Thai</h3>
    <p>The King and I Restaurant gives you a taste of Thai cuisine you're not likely to find outside of Thailand.</p>
  </div>
  <div class='col-sm-3'>
    <img alt='Map' height='250' width='100%'>
  </div>
  <div class='col-sm-12' id='people-list'>
    <span class='fa-stack fa-2x'>
      <i class="far fa-stack-2x fa-circle"></i>
      <i class="fas fa-stack-1x fa-user"></i>
    </span>
    <span class='fa-stack fa-2x'>
      <i class="far fa-stack-2x fa-circle"></i>
      <i class="fas fa-stack-1x fa-user"></i>
    </span>
    <span class='fa-stack fa-2x'>
      <i class="far fa-stack-2x fa-circle"></i>
      <i class="fas fa-stack-1x fa-user"></i>
    </span>
    <span class='fa-stack fa-2x'>
      <i class="far fa-stack-2x fa-circle"></i>
      <i class="fas fa-stack-1x fa-user"></i>
    </span>
  </div>
  <div class='col-sm-8 col-sm-offset-2' id='comments'>
    <div class='comment'>
      <span class='fa-stack fa-2x'>
        <i class="far fa-stack-2x fa-circle"></i>
        <i class="fas fa-stack-1x fa-user"></i>
      </span>
      Special mention to the Bangkok Kapow, the Spring Rolls, and the Red Curry Duck!
    </div>
    <div class='comment'>
      <span class='fa-stack fa-2x'>
        <i class="far fa-stack-2x fa-circle"></i>
        <i class="fas fa-stack-1x fa-user"></i>
      </span>
      We ordered crab rangoons and thai ravioli for appetizers-- definitely recommend them both!
    </div>
  </div>
</div>



<style>
  #brief-description {
    text-align: center;
  }

  #people-list {
    border-top: 1px solid black;
    border-bottom: 1px solid black;
    text-align: center;
    padding: 5px;
  }

  .comment {
    border: 1px solid black;
    padding: 5px;
    margin: 10px;
  }

  .profile-pic {
    height: 50px;
    width: 50px;
    border-radius: 50%;
  }

</style>